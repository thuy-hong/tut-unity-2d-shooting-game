﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public int speed = 1;
	public float lifeTime = 5;
	public int power = 1;
	
	void Start()
	{
		GetComponent<Rigidbody2D> ().velocity = transform.up.normalized * speed;
		Destroy (gameObject, lifeTime);
	}

	void Update()
	{
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2(1, 1));

		if (transform.position.y >= max.y - 0.5f) 
		{
			Destroy (gameObject);
		}
	}
}
