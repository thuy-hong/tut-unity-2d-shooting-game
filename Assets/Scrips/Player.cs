﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	// tốc độ di chuyển
	public float speed;

	Spaceship spaceship;

	IEnumerator Start()
	{
		spaceship = GetComponent<Spaceship> ();

		while (true) 
		{
			spaceship.Shot (transform);
			yield return new WaitForSeconds (spaceship.shotDelay);
		}
	}

	void Update ()
	{
		// phải - trái
		float x = Input.GetAxisRaw ("Horizontal");

		// trên - dưới
		float y = Input.GetAxisRaw ("Vertical");

		// lấy hướng di chuyển
		Vector2 direction = new Vector2 (x, y).normalized;

		Move (direction);
	}

	void Move(Vector2 direction)
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2(0, 0));

		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2(1, 1));

		Vector2 pos = transform.position;

		pos += direction * spaceship.speed * Time.deltaTime;

		pos.x = Mathf.Clamp (pos.x, min.x/2 + 0.5f, max.x/2 - 0.5f);
		pos.y = Mathf.Clamp (pos.y, min.y + 0.5f, max.y - 0.5f);

		transform.position = pos;
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		string layerName = LayerMask.LayerToName (c.gameObject.layer);

		if (layerName == "Bullet(Enemy)") 
		{
			Destroy (c.gameObject);
		}

		if(layerName == "Bullet(Enemy)" || layerName == "Enemy")
		{
			FindObjectOfType<Manager> ().GameOver ();
			spaceship.Explosion ();
			Destroy (gameObject);
		}
	}
}
